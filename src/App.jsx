import React, { useState } from "react";
import Home from "./pages/Home";
import { Global, css } from "@emotion/core";
import emotionNormalize from "emotion-normalize";
import ThemeContext, { defaultTheme } from "./context/theme";

const globalStyles = {
  html: {
    position: "relative",
    minHeight: "100%",
  },
  body: {
    minHeight: "100%",
    background: "black",
    fontFamily: "'Source Code Pro', monospace",
    paddingBottom: "50px",
  },
  a: {
    color: "inherit",
  },
};

const App = () => {
  const themeHook = useState(defaultTheme);

  return (
    <section>
      <Global
        styles={css`
          ${emotionNormalize}
          ${globalStyles}
        `}
      />
      <ThemeContext.Provider value={themeHook}>
        <Home />
      </ThemeContext.Provider>
    </section>
  );
};

export default App;
