import { useLayoutEffect, useState } from "react";

export function useDocumentSize() {
  const [size, setSize] = useState([0, 0]);

  useLayoutEffect(() => {
    const document = window.document.getElementsByTagName("html")[0];

    function updateSize() {
      setSize([document.offsetWidth, document.offsetHeight]);
    }

    window.addEventListener("resize", updateSize);
    updateSize();

    return () => window.removeEventListener("resize", updateSize);
  }, []);

  return size;
}
