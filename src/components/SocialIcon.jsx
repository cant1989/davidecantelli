import React, { useContext } from "react";
import styled from "@emotion/styled";
import ThemeContext from "../context/theme";
/** @jsx jsx */
import { css, jsx } from "@emotion/core";

const styles = (theme = "dark") => css`
  width: 40px;
  height: 40px;
  path {
    fill: ${theme === "light" ? "#000" : "#fff"} !important;
  }
`;

const Link = styled.a`
  display: block;
  text-transform: none;
  padding: 0 24px 24px 0;
  transition: all 0.3s;

  svg path {
    transition: all 0.2s;
  }

  &:hover {
    transform: scale(1.1);
  }

  &:hover svg path {
    fill: #01deb2 !important;
  }
`;

const SocialIcon = ({ icon: Icon, link }) => {
  const [theme] = useContext(ThemeContext);

  return (
    <Link href={link} target="_blank">
      <Icon css={styles(theme)} />
    </Link>
  );
};

export default SocialIcon;
