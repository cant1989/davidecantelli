import React from "react";
import styled from "@emotion/styled";

const StyledContainer = styled.div`
  max-width: 572px;
  margin: 0 auto;
  padding: 0 16px;
  overflow: hidden;
`;

const Container = ({ children }) => {
  return <StyledContainer>{children}</StyledContainer>;
};

export default Container;
