import React, { useRef, useEffect, useContext } from "react";
import { useDocumentSize } from "../hooks";
import styled from "@emotion/styled";
/** @jsx jsx */
import { jsx } from "@emotion/core";
import ThemeContext from "../context/theme";

const StyledCanvas = styled.canvas`
  position: absolute;
  top: 0;
  left: 0;
  z-index: -1;
  transition: background 0.3s;
`;

const createjs = window.createjs;
const TweenLite = window.TweenLite;
const Quad = window.Quad;
let stage = {};
let circles = [];

const Particles = ({ colors }) => {
  const canvasEl = useRef();
  const [docWidth, docHeight] = useDocumentSize();
  const [theme] = useContext(ThemeContext);

  useEffect(() => {
    if (stage && stage.canvas) {
      stage.canvas.width = docWidth;
      stage.canvas.height = docHeight;
    }
    resize();
  }, [docWidth, docHeight]);

  const initCircles = function() {
    const num_circles = docWidth * 0.7 > 300 ? 300 : docWidth * 0.7;

    for (let i = 0; i < num_circles; i++) {
      var circle = new createjs.Shape();
      const r = Math.floor(Math.random() * 4) + 1;
      const color = colors[Math.floor(i % colors.length)];

      circle.alpha = Math.random();
      circle.radius = r;
      circle.graphics.beginFill(color).drawCircle(0, 0, r);
      circle.x = docWidth * Math.random();
      circle.y = docHeight * Math.random();
      circles.push(circle);
      stage.addChild(circle);
      circle.movement = "float";

      tweenCircle(circle);
    }
  };

  useEffect(() => {
    circles.map((circle, i) => {
      const color = colors[Math.floor(i % colors.length)];
      circle.graphics.beginFill(color).drawCircle(0, 0, circle.radius);
    });
  }, [colors]);

  const tweenCircle = function(c) {
    if (c.tween) c.tween.kill();

    c.tween = TweenLite.to(c, 5 + Math.random() * 3.5, {
      x: c.x - 100 + Math.random() * 200,
      y: c.y - 100 + Math.random() * 200,
      ease: Quad.easeInOut,
      alpha: Math.random(),
      onComplete: function() {
        tweenCircle(c);
      }
    });
  };

  const animate = function() {
    stage.update();
    requestAnimationFrame(animate);
  };

  const resize = function() {
    if (stage && stage.canvas) {
      stage.canvas.width = docWidth;
      stage.canvas.height = docHeight;
      stage.removeAllChildren();
      stage.update();
      for (var i in circles) circles[i].tween.kill();
      initCircles();
    }
  };

  useEffect(() => {
    stage = new createjs.Stage("stage");
    initCircles();
    animate();
    stage.canvas &&
      (stage.canvas.width = docWidth) &&
      (stage.canvas.height = docHeight);
  }, []);

  return (
    <StyledCanvas
      css={{
        background: theme === "light" ? "white" : "black"
      }}
      id="stage"
      ref={canvasEl}
      width={docWidth}
      height={docHeight}
    />
  );
};

export default Particles;
