import React, { useContext } from "react";
import styled from "@emotion/styled";
import ThemeContext from "../context/theme";

const Label = styled.label`
  position: fixed;
  top: 36px;
  right: 36px;
  z-index: 5;
  display: inline-block;
  width: 52px;
  height: 26px;
`;

const Input = styled.input`
  opacity: 0;
  width: 0;
  height: 0;

  &:checked + span {
    background-color: #01deb2;

    &:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
    }
  }

  &:focus + span {
    box-shadow: 0 0 1px #2196f3;
  }
`;

const Span = styled.span`
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #000;
  border: 1px solid #ccc;
  border-radius: 34px;
  -webkit-transition: 0.4s;
  transition: 0.4s;

  &:before {
    position: absolute;
    content: "";
    height: 18px;
    width: 18px;
    left: 3px;
    top: 3px;
    background-color: white;
    border-radius: 50%;
    -webkit-transition: 0.4s;
    transition: 0.4s;
  }
`;

const Switcher = () => {
  const [theme, setTheme] = useContext(ThemeContext);
  console.log(theme);
  return (
    <Label>
      <Input
        type="checkbox"
        onChange={({ target }) => setTheme(target.checked ? "light" : "dark")}
        checked={theme === "light"}
      />
      <Span />
    </Label>
  );
};

export default Switcher;
