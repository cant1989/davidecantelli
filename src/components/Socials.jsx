import React from "react";
import styled from "@emotion/styled";
import SocialIcon from "./SocialIcon";
import { ReactComponent as GithubIcon } from "../assets/github.svg";
import { ReactComponent as LinkedinIcon } from "../assets/linkedin.svg";
import { ReactComponent as CodepenIcon } from "../assets/codepen.svg";
import { ReactComponent as UnsplashIcon } from "../assets/unsplash.svg";
import { ReactComponent as TelegramIcon } from "../assets/telegram.svg";
import { ReactComponent as WhatsappIcon } from "../assets/whatsapp.svg";
import { ReactComponent as MediumIcon } from "../assets/medium.svg";

/** @jsx jsx */
import { jsx } from "@emotion/core";

const Div = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 0 48px 16px;
`;

const Socials = () => {
  return (
    <Div>
      <SocialIcon icon={GithubIcon} link={"https://github.com/cant89"} />
      <SocialIcon
        icon={LinkedinIcon}
        link={"https://www.linkedin.com/in/davide-cantelli-55419b6b/"}
      />
      <SocialIcon icon={CodepenIcon} link={"https://codepen.io/cant89/"} />
      <SocialIcon icon={MediumIcon} link={"https://medium.com/@cant89/"} />
      <SocialIcon icon={UnsplashIcon} link={"https://unsplash.com/@cant89"} />
      <SocialIcon icon={TelegramIcon} link={"https://t.me/cant89"} />
    </Div>
  );
};

export default Socials;
