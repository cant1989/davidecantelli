import React from "react";
import styled from "@emotion/styled";

const Link = styled.a`
  color: #949494;
  font-size: 13px;
`;

const OldWebsiteLink = () => (
  <Link href="https://2014.davidecantelli.it/" target="_blank">
    Brave enough? Visit the 2014 website
  </Link>
);

export default OldWebsiteLink;
