import React from "react";
import styled from "@emotion/styled";

const style = `
display: flex;
background: rgb(3, 218, 220);
background: linear-gradient(
  0deg,
  rgba(3, 218, 220, 1) 0%,
  rgba(0, 223, 158, 1) 100%
);
padding: 12px 48px;
border: 0;
outline: none;
border-radius: 10px;
font-weight: 900;
font-size: 24px;
color: #fff;
position: absolute;
top: 100%;
left: 50%;
transform: translate3d(-50%, -50%, 0);
cursor: pointer;
transition: all 0.3s;
box-shadow: 0px 6px 26px 6px rgba(0, 0, 0, 0.21);
text-decoration: none;

&:hover {
  transform: translate3d(-50%, -60%, 0);
}
`;
const StyledButton = styled.button`
  ${style}
`;
const StyledLink = styled.a`
  ${style}
`;

const Button = ({ children, type = "button", ...rest }) => {
  const Comp = type === "button" ? StyledButton : StyledLink;
  return <Comp {...rest}>{children}</Comp>;
};

export default Button;
