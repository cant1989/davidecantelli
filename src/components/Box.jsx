import React, { useContext } from "react";
import styled from "@emotion/styled";
import Button from "./Button";
import CV from "../assets/davide-cantelli_cv_2024.05.pdf";
import { ReactComponent as DownloadIcon } from "../assets/download.svg";
/** @jsx jsx */
import { jsx } from "@emotion/core";
import ThemeContext from "../context/theme";

const StyledBox = styled.div`
  margin-bottom: 64px;
  padding: 24px 40px 40px;
  border-radius: 10px;
  background: #fff;
  border-width: 2px;
  border-style: solid;
  color: #515151;
  font-size: 16px;
  font-weight: 500;
  position: relative;
  line-height: 28px;
  transition: all 0.3s;
`;

const downloadIconStyles = {
  height: "28px",
  marginRight: "16px",
};

const Box = ({ children, onButtonMouseEnter, onButtonMouseLeave }) => {
  const [theme] = useContext(ThemeContext);

  return (
    <StyledBox
      css={{
        borderColor: theme === "light" ? "#000" : "#fff",
      }}
    >
      {children}
      <Button
        type="link"
        href={CV}
        target="_blank"
        onMouseEnter={onButtonMouseEnter}
        onMouseLeave={onButtonMouseLeave}
      >
        <DownloadIcon css={downloadIconStyles} /> CV/RESUME
      </Button>
    </StyledBox>
  );
};

export default Box;
