import React, { Fragment, useState, useContext } from "react";
import styled from "@emotion/styled";
import Particles from "../components/Particles";
import Box from "../components/Box";
import Container from "../components/Container";
import Switcher from "../components/Switcher";
import Socials from "../components/Socials";
import OldWebsiteLink from "../components/OldWebsiteLink";
/** @jsx jsx */
import { jsx } from "@emotion/core";
import ThemeContext from "../context/theme";

const Title = styled.h1`
  font-size: 72px;
  color: #fff;
  font-weight: 900;
  margin: 64px 48px 0;
  transition: all 0.3s;
`;

const Subtitle = styled.h2`
  font-size: 30px;
  color: #fff;
  font-weight: 900;
  margin: 8px 48px 32px;
  transition: all 0.3s;
`;

const Footer = styled.footer`
  position: absolute;
  bottom: 0;
  right: 0;
  height: 18px;
  display: block;
  margin: 16px;
  text-align: right;
`;

const COLORS_ORIGINAL = ["#d51b11", "#2ecc71", "#FDC162", "#7BB7FA"];
const COLORS_GRADIENT = ["#01de9f", "#01ddb1", "#03dccb", "#04dad9"];

const Home = () => {
  const [buttonHover, setButtonHover] = useState(false);
  const [theme] = useContext(ThemeContext);

  return (
    <Fragment>
      <Particles colors={buttonHover ? COLORS_GRADIENT : COLORS_ORIGINAL} />
      <Switcher />
      <Container>
        <Title css={{ color: theme === "light" ? "#000" : "#fff" }}>Hi!</Title>
        <Subtitle css={{ color: theme === "light" ? "#000" : "#fff" }}>
          I am Davide Cantelli
        </Subtitle>
        <Socials />
        <Box
          onButtonMouseEnter={() => setButtonHover(true)}
          onButtonMouseLeave={() => setButtonHover(false)}
        >
          <p>
            A Software Developer greatly focused on the front-end,
            passionate about design and photography, detail and problem-solving
            oriented.
          </p>
          <p>
            Currently living in Bologna and working remotely for{" "}
            <a href="https://trustlayer.io/">TrustLayer</a>.
          </p>
        </Box>
      </Container>
      <Footer>
        <OldWebsiteLink />
      </Footer>
    </Fragment>
  );
};

export default Home;
