import React from "react";

export const defaultTheme =
  window.matchMedia &&
  window.matchMedia("(prefers-color-scheme: light)").matches
    ? "light"
    : "dark";

const ThemeContext = React.createContext([defaultTheme, () => {}]);
export default ThemeContext;
