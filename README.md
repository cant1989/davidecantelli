# Davide Cantelli personal website

## First setup

Install firebase cli

```
npm i -g firebase-tools
```

Login to firebase and connect to the existing project

```
firebase login
```

Install all dependencies

```
npm i
```

## Work

```
npm start
```

## Deploy

```
npm run deploy
```
